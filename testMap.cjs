//Test array
const items = ["1", "2", "3"];

//Call back that is to be passed to each function
function modify(element, idx, items){
    if(idx < 2){
        return element * 5;
    }
}

//Importing map function
const mapFunc = require('./map.cjs');
const resArr = mapFunc(items, parseInt);

//Consoling the output
console.log(resArr);