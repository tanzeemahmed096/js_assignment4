//Test array
const items = [1, 2, 3, 4, 5, 5];

//Call back that is to be passed to each function
function modify(accuVal, currentVal){
    return Math.min(accuVal, currentVal);
}

let initialVal = 2;
//Importing reduce function
const reduceFunc = require('./reduce.cjs');
const resArr = reduceFunc(items, modify, initialVal);

//Consoling the output
console.log(resArr);