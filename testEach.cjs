//Test array
const items = [1, 2, 3, 4, 5, 5];

//Call back that is to be passed to each function
function modify(element, idx){
    return element * 5;
}

//Importing each function
const eachFunc = require('./each.cjs');
const resArr = eachFunc(items, modify);

//Consoling the output
console.log(resArr);