//Creating each function
module.exports = function each(items, callbackFunc){
    //If no array provided or no callback function provided return [];
    if(arguments.length === 0) return [];

    //iterating over items array
    for(let idx = 0; idx < items.length; idx++){
        //storing a return value in a variable
        const element = callbackFunc(items[idx], idx);

        //Checking if the funtion returns a value or a undefined
        if(!element){
            items[idx] = items[idx];
        }else{
            items[idx] = element;
        }
    }

    //returing the modified array
    return items;
}