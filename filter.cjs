//Creating a filter function
module.exports = function filter(items, callbackFunc){
    //array which will store the elements which for which callback function return true
    const arr = [];

    //Iterating over items array to check the condition
    for(let idx = 0; idx < items.length; idx++){
        //Pushing the elements to array if the condition is true
        if(callbackFunc(items[idx], idx, items) === true) arr.push(items[idx]);
    }

    //returning the modified array
    return arr;
}