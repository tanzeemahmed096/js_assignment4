//Creating map function
module.exports = function map(items, callbackFunc) {
  //If no array provided or no callback function provided return [];
  if (arguments.length === 0) return [];
  //Copying the original array
  const copyArr = [...items];

  //iterating over copyArr array
  for (let idx = 0; idx < copyArr.length; idx++) {
    //storing a return value in a variable
    let element = callbackFunc(items[idx], idx, items);
    copyArr[idx] = element;
  }

  //returing the modified array
  return copyArr;
};
