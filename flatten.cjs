//Creating flatten function
module.exports = function flatten(nestedArr, depth) {
  //If depth level is not given then assume depth as 1
  if(!depth) depth = 1;

  //Array which will store 2d array in the form of 1d array;
  const resArr = recursiveFlat(nestedArr, depth);

  //return the result
  return resArr;
};

function recursiveFlat(nestedArr, depth) {
  return depth > 0 ? nestedArr.reduce((acc, val) => 
        acc.concat(Array.isArray(val) ? recursiveFlat(val, depth - 1) : val),[]) : nestedArr.slice();
};