//Test array
const items = [1, 2, 3, 4, 5, 5];

//Call back that is to be passed to each function
function modify(element, idx, items){
    return element === 4;
}

//Importing find function
const findFunc = require('./filter.cjs');
const resArr = findFunc(items, modify);

//Consoling the output
console.log(resArr);