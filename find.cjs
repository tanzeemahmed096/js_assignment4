//Creating find function
module.exports = function find(items, callbackFunc){
    //Iterating over items array to find the element
    for(let idx = 0; idx < items.length; idx++){
        if(callbackFunc(items[idx], idx)) return items[idx];
    }
}