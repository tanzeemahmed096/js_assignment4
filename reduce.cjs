//Creating reduce function
module.exports = function reduce(items, callbackFunc, initialVal){
    // if there is only one element in array then return that
    if(items.length === 1) return items[0];

    //if initial value is not provided then taking first element as initial value
    if(!initialVal){
        initialVal = items[0];

        //iterating and performing operation on the array from first index
        for(let idx = 1; idx < items.length; idx++){
            // updating initialVal as accumulator
            initialVal = callbackFunc(initialVal, items[idx], idx, items);
        }

        return initialVal;
    }

    // if initialVal is given the iterating from 0 idx
    if(initialVal){
        for(let idx = 0; idx < items.length; idx++){
            // updating initialVal as accumulator
            initialVal = callbackFunc(initialVal, items[idx], idx, items);
        }
        return initialVal;
    }

    return 0;
}